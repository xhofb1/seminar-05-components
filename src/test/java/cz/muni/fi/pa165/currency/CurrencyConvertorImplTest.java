package cz.muni.fi.pa165.currency;


import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;

import java.math.BigDecimal;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CurrencyConvertorImplTest {

    private Currency EUR = Currency.getInstance("EUR");
    private Currency CZK = Currency.getInstance("CZK");

    @Mock
    private ExchangeRateTable ert;
    private CurrencyConvertor cc;


    @BeforeEach
    void setup(){
        cc = new CurrencyConvertorImpl(ert);
    }

    @Test
    public void testConvert() throws ExternalServiceFailureException {
        // Don't forget to test border values and proper rounding.
        when(ert.getExchangeRate(EUR, CZK))
                .thenReturn(new BigDecimal("0.1"));

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(cc.convert(EUR, CZK, new BigDecimal("10.050")))
                    .isEqualTo(new BigDecimal("1.00"));
            softly.assertThat(cc.convert(EUR, CZK, new BigDecimal("10.051")))
                    .isEqualTo(new BigDecimal("1.01"));
            softly.assertThat(cc.convert(EUR, CZK, new BigDecimal("10.149")))
                    .isEqualTo(new BigDecimal("1.01"));
            softly.assertThat(cc.convert(EUR, CZK, new BigDecimal("10.150")))
                    .isEqualTo(new BigDecimal("1.02"));
        });

    }

    @Test
    public void testConvertWithNullSourceCurrency() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> cc.convert(null, CZK, BigDecimal.ONE));
    }

    @Test
    public void testConvertWithNullTargetCurrency() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> cc.convert(EUR, null, BigDecimal.ONE));
    }

    @Test
    public void testConvertWithNullSourceAmount() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> cc.convert(EUR, CZK, null));
    }

    @Test
    public void testConvertWithUnknownCurrency() throws  ExternalServiceFailureException {
        when(ert.getExchangeRate(EUR, CZK))
                .thenReturn(null);
        assertThatExceptionOfType(UnknownExchangeRateException.class)
                .isThrownBy(() -> cc.convert(EUR, CZK, BigDecimal.ONE));

    }

    @Test
    public void testConvertWithExternalServiceFailure() throws ExternalServiceFailureException{
        when(ert.getExchangeRate(EUR, CZK))
                .thenThrow(ExternalServiceFailureException.class);
        assertThatExceptionOfType(UnknownExchangeRateException.class)
                .isThrownBy(() -> cc.convert(EUR, CZK, BigDecimal.ONE));


    }

}
