package cz.muni.fi.pa165.currency;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;


/**
 * Base implementation of {@link CurrencyConvertor}.
 *
 * @author petr.adamek@embedit.cz
 */
public class CurrencyConvertorImpl implements CurrencyConvertor {

    private final ExchangeRateTable exchangeRateTable;
    private final static Logger log = LoggerFactory.getLogger(CurrencyConvertorImpl.class);

    // uncomment and import classes from org.slf4j package
    //private final Logger logger = LoggerFactory.getLogger(CurrencyConvertorImpl.class);

    public CurrencyConvertorImpl(ExchangeRateTable exchangeRateTable) {
        this.exchangeRateTable = exchangeRateTable;
    }


    @Override
    public BigDecimal convert(Currency sourceCurrency, Currency targetCurrency, BigDecimal sourceAmount) throws IllegalArgumentException, UnknownExchangeRateException {
        log.trace("convert {},{},{}", sourceCurrency, targetCurrency, sourceAmount);
        if (sourceCurrency == null) {
            throw new IllegalArgumentException("Source currency is null");
        }
        if (targetCurrency == null) {
            throw new IllegalArgumentException("Target currency is null");
        }
        if (sourceAmount == null) {
            throw new IllegalArgumentException("Source amount is null");
        }
        try {
            BigDecimal exchangeRate = exchangeRateTable.getExchangeRate(sourceCurrency, targetCurrency);
            if (exchangeRate == null) {
                log.warn("missing exchange rate for given currencies");
                throw new UnknownExchangeRateException("ExchangeRate is unknown");
            }
            return exchangeRate.multiply(sourceAmount).setScale(2, RoundingMode.HALF_EVEN);
        } catch (ExternalServiceFailureException ex) {
            log.error("ExternalServiceFailureException");
            throw new UnknownExchangeRateException("Error when fetching exchange rate", ex);
        }
    }

}
